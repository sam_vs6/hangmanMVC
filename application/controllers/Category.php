<?php
	class Category extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->model('Word_model');

			
		}
		 public function index(){
		 	$data['segmento'] = $this->uri->segment(3);
		 	if(!$data['segmento']){
		 		header("Location: categories");
		 		exit();
			}
			else
			{
		 		$data['words'] = $this->Word_model->getWord($data['segmento']);
			}
			$this->load->view("/headers");
			$this->load->view("category/index", $data);
		 }

		 public function create()
		 {
		 	$newWord = $this->input->post('newWordTextBox');
		 	$categoryId= $this ->input->post('categoryId');
			$lastId = $this->Word_model->create($newWord,$categoryId);
			$this->load->helper('url');
			redirect("category/index/".$categoryId);
		 }
		 public function delete()
		 {
		 	 $data['segmento'] = $this->uri->segment(3);
		 	 $categoryId= $this ->input->post('categoryId');
		 	 $wordsId= $this ->input->post('wordsId');
		 	 $this->Word_model->delete($wordsId);
		 	 $this->load->helper('url');
			 redirect("category/index/".$categoryId);

		 }
	}
?>