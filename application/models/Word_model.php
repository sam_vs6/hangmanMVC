<?php
class Word_model extends CI_Model{
	public function __construct()
	{
		$this->load->database();
	}
	public function getWord($Categoriesid)
	{
		$this->db->where('categoryId',$Categoriesid);
		$query=$this->db->get('word');
		if($query->num_rows()>=0) 
		{
			return $query->result_array();
		}
		else
		{ 
			return false;
		}
	}

	public function create($word, $categoryId)
	{
		$data = array ('text'=> $word, 'categoryId' => $categoryId);
		$this->db->insert('word',$data);
		return $this->db->insert_id();
	}

    public function delete($id)
    {
    	$this->db->delete("word",array('id'=>$id));
    }
}
?>