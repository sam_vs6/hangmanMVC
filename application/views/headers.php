<!DOCTYPE html>
<html lang="es-BO">
  <head>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    
    <title>Hangman MVC</title>
	<meta charset="UTF-8">
	<style type="text/css"> 
		h1{text-align: center;}
		.campos{text-align: center;}
	</style>
  </head>
    <body>
  	<nav class="navbar navbar-static-top">
	  <!-- Navbar Right Menu -->
	  <div class="navbar-custom-menu">
	    <ul class="nav navbar-nav">
	      <li>
	        <a href="/home/index"><i class="fa fa-home"></i>&nbsp;Inicio</a>
	      </li>
	      <li>
	        <a href="/categories"><i class="fa fa-user"></i>&nbsp;Categorias</a>
	      </li>
	      <li>
	        <a href="/home/about"><i class="fa fa-archive"></i>&nbsp;Acerca de</a>
	      </li>
	    </ul>
	  </div>
	</nav>