

    <div class="container">
    	<h1 class="jumbotron">Categorías</h1>
    	<form method="POST" action="/categories/create">
    		<p>Categoria Nueva</p>
		  	<input type="text" name="newCategoryTextBox" maxlength="50"/> &nbsp;&nbsp;&nbsp;
		  	<input class="btn btn-success" type="submit" name="createNewCategoryButton" value="Crear" />
		</form>
	</div>
	<ul>
      <div class="container">
	  <table class="table">
	  <tr>
	  	   <th class="text-center">Categoria</th>
	  	   
	  </tr>
	  <?php foreach ($categories as $category): ?>
	  <form method="POST" action="/categories/delete">	 
	  <input type="hidden" name="categoriesIDs" value="<?=$category["id"]?>"/>
	  <tr>
		  <td><li><a href="category/index/<?= $category["id"] ?>"><?= $category["name"]?></a></li></td>
		  <td><input class="btn btn-warning" type="submit" name="delete" value="eliminar"/></td>
	  </tr>
	  </form>
	  <?php endforeach; ?>
    	
	  </table>
	  </div>
	</ul>
  </body>
</html>
