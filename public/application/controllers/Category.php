<?php class Category extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Word_model');

		
	}
	 public function index(){
	 	$data['segmento'] = $this->uri->segment(3);
	 	if(!$data['segmento']){
	 		header("Location: categories");
	 		exit();
		}
		else
		{
	 		$data['words'] = $this->Word_model->getWord($data['segmento']);
		}
		$this->load->view("category/index", $data);
	 }

	 public function create()
	 {
	 	$newWordName = $this->input->post('newWordTextBox');
	 	$categories_id = $this->uri->segment(4);
		$lastId = $this->Word_model->createWords($newWordName,$categories_id);
		$this->load->helper('url');
		redirect('category');
	 }

	 public function getSegement(){
	 	$data = $this->uri->segement(2);
	 	return $data;
	 }
}
?>